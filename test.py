from Praktika5 import *
import pytest

""" Тесты-pytest'ы """
def test_inp_valid():
    """
    Рассматривается работа функции inp_valid()

    Происходит тестирование на корректных значениях, а так же на trigger ошибки
    ValueError
    """
    assert inp_valid([1,2,1,4]) == [1,2,1,4]
    assert inp_valid([1,8,2,4]) == [1,8,2,4]
    with pytest.raises(ValueError):
        assert inp_valid([1])
        assert inp_valid([1,0,1,1])
        assert inp_valid([1,9,1,1])
        assert inp_valid([1,1.1,1,1])

def test_horse():
    """
    Рассматривается работа функции horse()

    Происходит тестирование на корректных значениях различных положений
    конечной точки
    """
    assert horse(1, 2, 5, 6) == 4
    assert horse(1, 1, 5, 6) == 3
    assert horse(1, 1, 1, 2) == 3
    assert horse(1, 1, 1, 3) == 2
    assert horse(1, 1, 2, 3) == 1
    assert horse(1, 1, 1, 4) == 0
    assert horse(1, 1, 1, 1) == 0

def test_horse_2_setup():
    """
    Рассматривается работа функции horse_2_step()

    Происходит тестирование на корректных значениях различных положений
    конечной точки
    """
    assert horse_2_step(1, 5) == [2, 2]
    assert horse_2_step(1, 6) == [2, 2, 1]

def test_king():
    """
    Рассматривается работа функции king()

    Происходит тестирование на корректных значениях различных положений
    конечной точки
    """
    assert king(1, 1, 2, 3) == 2
    assert king(1, 1, 8, 8) == 7
    assert king(1, 1, 1, 1) == 0

def test_elephant():
    """
    Рассматривается работа функции elephant()

    Происходит тестирование на корректных значениях различных положений
    конечной точки
    """
    assert elephant(1, 1, 2, 3) == 0
    assert elephant(1, 1, 1, 3) == 2
    assert elephant(1, 1, 8, 8) == 1
    assert elephant(1, 1, 1, 1) == 0

def test_castle():
    """
    Рассматривается работа функции castle()

    Происходит тестирование на корректных значениях различных положений
    конечной точки
    """
    assert castle(1, 1, 2, 3) == 2
    assert castle(1, 1, 1, 8) == 1
    assert castle(1, 1, 1, 1) == 0
