""" Практическая работа №5
  Домнин Данил КИ19-17/2Б
Вариант 21: О шахматах """

import os
import pytest


""" Функции """
def inp_valid(a):
    """
    Проверка пользовательского ввода

    Проверяет введённыые координаты пользователем

    Args:
    # Все координаты ограничены стандартной шахматной доской [1; 8]
        a: массив вида [x1, y1, x2, y2] с координатами начала и конца
    Return:
        введённый массив вида [x1, y1, x2, y2]
    Raise:
        ValueError

    Examles:
        >>> inp_valid([1, 2, 3, 4])
        [1, 2, 3, 4]
        >>> inp_valid([1, 2])
        ValueError("Неверное количество координат")
        >>> inp_valid([1, 2, 10000, a])
        ValueError(("Неверное значение координаты"))
    """
    if len (a) != 4:
        raise ValueError("Неверное количество координат")
    try:
        for _ in range(4):
            i = int(a.pop(0))
            if not 1 <= i <= 8:
                 raise ValueError
            a.append(i)
    except ValueError:
        raise ValueError("Неверное значение координаты")
    return a


"""Конёк-горбунёк"""
def horse_2_step(x, a): # Определяет минимальное количество ходов по х и y
    """
    Вспомогательное для коня

    Производит разбиение перемещения по одной из оси координат на числа 2 и 1 -
    на возможные перемещения коня по оси за ход

    Args:
    # Все координаты ограничены стандартной шахматной доской [1; 8]
        x: координата начального положения по одной из осей
        a: координата конечного положения по той же оси
    Return:
        Массив возможных ходов коня вида [2, 2, ..., 2, 1]
    Raise:
        Нет

    Examles:
        >>> horse_2_step(1, 5)
        [2, 2]
        >>> horse_2_step(1, 6)
        [2, 2, 1]
    """
    var_ = []
    k = abs(x - a)
    while k >= 2:
        k -= 2
        var_.append(2)
    if k == 1:
        var_.append(1)
    # print(var_)
    return var_

def horse(x, y, a, b): # Определяет возможность тыгыдыка
    """
    Расчёт коня

    Считает количество ходов для фигуры конь на основе разбиения ходов по
    каждой оси массив из двоек и едениц (функция horse_2_step()) и компановке
    их в ходы по типу (2, 1) и (1, 2)

    Args:
    # Все координаты ограничены стандартной шахматной доской [1; 8]
        x: x начала
        y: y начала
        a: x конца
        b: y конца
    Return:
        количество ходов
    Raise:
        Нет

    Examles:
        >>> horse(1, 2, 5, 6)
        4
        >>> horse(1, 1, 1, 2)
        3
        >>> horse(1, 1, 1, 1)
        0
    """
    var_x = horse_2_step(x, a)
    var_y = horse_2_step(y, b)
    var_x.reverse()
    var_y.reverse()
    # Пытаемся уровнять количество ходов по х;у
    d = 0
    t = 0
    while t < 2:
        print(var_x, var_y)
        # Отбрасываем готовые ходы:
        while 1 in var_x and 2 in var_y:
            var_x.pop(0)
            var_y.pop(-1)
            d +=1
            t = 0
        while 2 in var_x and 1 in var_y:
            var_x.pop(-1)
            var_y.pop(0)
            d +=1
            t = 0
        print('---', var_x, var_y)
        # Несколько базовых ходов:
        if var_x == [] or var_y == []:
            if  var_x == [] and var_y == [1] \
              or var_x == [1] and var_y == []:
                d += 3
                var_x = var_y = []
            if  var_x == [] and var_y == [2] \
              or var_x == [2] and var_y == []:
                d += 2
                var_x = var_y = []
        # Превращаем в ходы:
        if 2 in var_x:
            if var_y.count(2) >= 2:
                var_x.pop(-1)
                var_x.insert(0, 1)
                var_x.insert(0, 1)
            elif var_y == []:
                var_y.append(1)
                var_y.append(1)
            elif var_x.count(2) >= 2:
                var_y.pop(-1)
                var_y.insert(0, 1)
                var_y.insert(0, 1)
        elif 1 in var_x:
            if var_y == []:
                var_y.append(2)
                var_y.append(2)
            elif var_x.count(1) >=2:
                var_x.pop(0, 1)
                var_x.pop(0, 1)
                var_x.append(2)
            elif var_y.count(1) >=2:
                var_y.pop(0, 1)
                var_y.pop(0, 1)
                var_y.append(2)
        elif var_x == []:
            if 2 in var_y:
                var_x.append(1)
                var_x.append(1)
            elif 1 in var_y:
                var_x.append(2)
                var_x.append(2)
        t += 1
    if var_x != [] and var_y != []:
         d = 0
    return d
"""Сделал тыгыдык """


def king(x, y, a, b):
    """ Расчёт короля

    Считает количество ходов для фигуры король

    Args:
    # Все координаты ограничены стандартной шахматной доской [1; 8]
        x: x начала
        y: y начала
        a: x конца
        b: y конца
    Return:
        количество ходов
    Raise:
        Нет

    Examles:
        >>> king(1, 1, 2, 3)
        2
        >>> king(1, 1, 8, 8)
        7
    """
    d = (int(abs(x - a)), int(abs(y - b)))
    if max(d) != 0:
        d = max(d)
    else:
        d = min(d)
    return d

def elephant(x, y, a, b):
    """ Расчёт слона

    Считает количество ходов для фигуры слон

    Args:
    # Все координаты ограничены стандартной шахматной доской [1; 8]
        x: x начала
        y: y начала
        a: x конца
        b: y конца
    Return:
        количество ходов
    Raise:
        Нет

    Examles:
        >>> elephant(1, 1, 2, 3)
        0
        >>> elephant(1, 1, 1, 3)
        2
        >>> elephant(1, 1, 8, 8)
        1
    """
    if x == a and y == b:
        d = 0
    elif abs(x - a) == abs(y - b):
        d = 1
    elif not (x + y)%2 == (a + b)%2:
        d = 0
    else:
        d = 2
    return d

def castle(x, y, a, b):
    """
    Расчёт ферзя

    Считает количество ходов для фигуры ферзь

    Args:
    # Все координаты ограничены стандартной шахматной доской [1; 8]
        x: x начала
        y: y начала
        a: x конца
        b: y конца
    Return:
        количество ходов
    Raise:
        Нет

    Examles:
        >>> castle(1, 1, 2, 3)
        2
        >>> castle(1, 1, 1, 8)
        1
    """
    if x == a and y == b:
        d = 0
    elif x == a or y == b:
        d = 1
    else:
        d = 2
    return d

""" Начало программы """
if __name__ == "__main__":
    print("Введите положение фигуры и её конечное положение через пробелы:\n\
               exit - выход\n\
               test - запуск тестов")

    # Реалицация пользовательского ввода
    while True:
        try:
            a = input().split(' ')
            if a[0] == "test":
                os.system('python -m pytest test.py -v')
                exit()
            if a[0] == "exit":
                exit()
            if a[0] != "exit" and "test":
                a = inp_valid(a)
        except ValueError as e:
            print(e)
            print("Попробуйте снова :)")
        else:
            break

    # x, y, a, b = a
    print(horse(*a), king(*a), castle(*a), elephant(*a))
